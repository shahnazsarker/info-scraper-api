
<?php

$url= $_GET['domainName'];
$url= 'https://' . $url;

if(isset($url)){


    $ch = curl_init();
    //$url= "http://genvio.com.bd";
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 1);

    $response = curl_exec($ch);

    // Return headers seperately from the Response Body

    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $headers = substr($response, 0, $header_size);

    curl_close($ch);
    $headers_indexed_arr = explode("\r\n", $headers);
    //print_r($headers_indexed_arr);
    // Define as array before using in loop
    $headers_arr = array();
    // Remember the status message in a separate variable
    $status_message = array_shift($headers_indexed_arr);


    foreach ($headers_indexed_arr as $value) {
        if(false !== ($matches = explode(':', $value, 2))) {
            $headers_arr["{$matches[0]}"] = trim($matches[1]);
            //print_r($response);
        }
    }

        //    print_r($headers_arr);
        //    die();
    header('content-type: application/json; charset=utf-8');
    $result = array();
    $result["Domain Name"] = $url;
    $result ["Server"] = $headers_arr["server"];

    $result["SSL"] = $headers_arr["alt-svc"] ? "Yes" : "No";

    $result["CMS"] = $headers_arr["x-generator"] ? "Yes" : "No";
    echo json_encode($result);
    exit();
}