# Info Scraper API with PHP
An API that scrapes informaion of given domain name. It provides domain's Server name, whether SSL key available or not and CMS information.
## Info Scraper API :
* Takes Domain Name as input.
* Prints the server name of the domain.
* prints whether CMS & SSL key available or not.
## Flowchart of the Info Scraper API

![infoscraper-API-flowchart](/uploads/588872cda7270e07e2ba6567a2506fa3/infoscraper-API-flowchart.png)




